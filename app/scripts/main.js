'use strict';

(function($) {

  // VARIABLES

  var Highcharts = require('highcharts');
  require('highcharts/modules/exporting')(Highcharts)
  require('highcharts/modules/sankey')(Highcharts);
  var myChart = Highcharts;  
 

  // INIT

  loadJSON('data1.json'); // load init dataset
 


  // FUNCTIONS

  function loadJSON(json) {
    
     $.when($.ajax('ajax/' + json))
      .then(data => {

         if(myChart.charts.length == 0){
            createHC(data); // create highchart once
         } else {
            myChart.charts[0].series[0].setData( data ); // then only update
         };

      })
      .catch(err => { console.dir(err) })
  }

  function createHC(d) {
    
    /* Draws the Highcharts graphic */
      Math.linear = function(pos) {return pos;};

        myChart.chart('myHC', {
            exporting: {
              buttons: {
                  contextButton: {
                      enabled: false
                  },
                  exportButton: {
                      text: 'Download',
                      menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.splice(2)
                  },
                  printButton: {
                      text: 'Print',
                      onclick: function () {
                          this.print();
                      }
                  }
              }
          },
          chart: {
            height: 600
          },
          title: {
            text: 'Highcharts Sankey Diagram'
          },
          credits: { enabled: false},
          legend: { enabled: false},
          // exporting: { enabled: false },
          series: [{
            type: 'sankey',
            keys: ['from', 'to', 'weight'],
            data: d,
            nodes: [{
                id: 'Electric Generation',
                offset: -180,
                column: 1
            }, {
                id: 'Residential',
                column: 2
            }, {
                id: 'Commercial',
                column: 2
            }, {
                id: 'Industrial',
                column: 2
            }, {
                id: 'Transportation',
                column: 2
            }, {
                id: 'Rejected Energy',
                column: 3
            }
            ],
            name: 'Sankey demo series'
          }]
      })
  }

  function changeData(dd){
    myChart.charts[0].series[0].setData( dd );
  }


  // EVENT HANDLERS

  $('#btn-data1').click(function(event) {
    event.preventDefault();    
    loadJSON('data1.json');
  });
  $('#btn-data2').click(function(event) {
    event.preventDefault();  
    loadJSON('data2.json');
  });
	$('#btn-data3').click(function(event) {
		event.preventDefault();
    loadJSON('data3.json');
	});


})(jQuery);
